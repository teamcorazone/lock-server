// 
// SYSTEM VARIABLES
// ------------------
var sys = require('sys')
var exec = require('child_process').exec;
function puts(error, stdout, stderr) { sys.puts(stdout) }

//
// SERVER
// ------------------
var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

// @TODO: Lees de status uit, ipv het te setten.
var locked = true; // Start met een gesloten slot 

//
// SOCKET
// ------------------

// Wanneer er een connectie is ontstaan, wordt de onderste code uitgevoerd.
io.on('connection', (socket) => {
	console.log("client connected");
	
	// Stuur de connectie status terug naar de verbonden client.
	socket.emit('connectionState', {
		connectionState: 'connected'
	});
	
	// CLIENT AUTHENTICATES
	socket.on('authenticate', function(data){

		// Authenticeer, en controleer of dat een slot gesloten is
		if (data.password == 'password123' /*@TODO: set password */ && locked) {
			console.log('authenticated');
			
			// Vertel de client dat het slot open gaat
			socket.emit('lockState', {
				lockState: 'open'
			});
			
			// Voer het python script uit dat het slot opent
			exec("sudo python ./python/unlock.py", puts);
			
			// Zet de status van het slot op open
			locked = false;
		}
	});
	
	// CLIENT LOCKS
	socket.on('lock', function(data){

		// Als de client het slot op slot wil, en het is niet gesloten, voer dan de rest uit.
		if (data.lock == true && !locked) {		
			console.log('locked');	
			
			// Zeg tegen de client dat het slot op slot gaat.
			socket.emit('lockState', {
				lockState: 'locked'
			});
			
			// Voer het script dat het slot daadwerkelijk op slot zet uit.
			exec("sudo python ./python/lock.py", puts);
			
			// Zet de status van het slot op gesloten
			locked = true;
		}
	});
	
	// CLIENT DISCONNECTS
	socket.on('disconnect', function(){
		console.log("client disconnected");
		
		// Als het slot niet op slot staat, zet hem dan op slot
		if (!locked) {
			// Voer het script uit dat het slot op slot zet.
			exec("sudo python ./python/lock.py", puts);
			
			// Zet de status van het slot op gesloten
			locked = true;
		}
		
		// Vertel de client dat de socket gesloten wordt.
		socket.emit('connectionState', {
			connectionState: 'disconnected'
		});
	});
});

// START SERVER
server.listen(3000);
